#!/usr/bin/env node

// Node Modules
const childProcess = require("child_process");
const path = require("path");
const chalk = require("chalk").default;

const file = process.argv[2];

const launch = () => {
  let child = childProcess.spawn("node", [path.resolve(__dirname, file)], {
    stdio: [0, 1, 2, "ipc"]
  });
  child.on("error", err => console.log(chalk.red("Error: "), err.stack));

  child.on("message", msg => {
    if (msg === "restart") {
      killAndRestart(child);
    }
  });

  child.on("close", () => {
    child.removeAllListeners();
    child.unref();
    child = launch();
    console.log(chalk.blue("Restart:"), `New Process ID: ${child.pid}`);
  });

  return child;
};

const killAndRestart = proc => {
  proc.kill("SIGINT");
};

launch();
