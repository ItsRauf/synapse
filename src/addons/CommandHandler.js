const Logger = require("./Logger");
const config = require("../private/config.json");

module.exports = (msg, Synapse, GuildPrefix) => {
  // Check if message starts with preix
  if (
    msg.content.startsWith(GuildPrefix) ||
    msg.content.startsWith(config.prefix)
  ) {
    let split = msg.content.split(" ");
    let cmdName =
      split[0].slice(GuildPrefix.length) ||
      split[0].slice(config.prefix.length);
    // Shift to remove 1st element (command name)
    split.shift();
    let args = split;
    // Check if command is valid
    if (Synapse.commands.has(cmdName)) {
      // Get Command
      let cmd = Synapse.commands.get(cmdName);
      // Run Command
      cmd.func(msg, args, Synapse);
    }
  } else {
    return;
  }
};

module.exports.loadCmds = Synapse => {
  const fs = require("fs");
  // Read Command Folder
  fs.readdir("./src/cmds", async (err, files) => {
    if (err) return Logger.error(err.stack);
    for (let file in files) {
      file = files[file];
      let cmd = require(`../cmds/${file}`);
      // Register Command
      Synapse.commands.set(cmd.name, cmd);
      Logger.info(`Command: ${cmd.name}`);
    }
  });
};
