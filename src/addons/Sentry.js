const Sentry = require("@sentry/node");
const config = require("../private/config.json");
const chalk = require("chalk").default;

if (process.env.DEVELOPMENT) {
  Sentry.init({
    dsn: config.SentryDSN
  });
  console.log(`${chalk.blue.bold("Info:")} Connected to Sentry`);
}

module.exports = Sentry;
