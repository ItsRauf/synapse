const config = require("../private/config.json");
const { Message, Client } = require("eris");

/**
 * Callback Function for Commands
 *
 * @callback cmdFunc
 * @param {Message} msg
 * @param {String[]} args
 * @param {Client} Synapse
 */

/**
 * Command:
 * Executable snippit of code.
 * @class Command
 */
class Command {
  /**
   * Creates an instance of Command.
   * @param {String} name
   * @param {cmdFunc} func
   * @param {Object} help
   * @param {String} help.description
   * @param {String | String[]} help.usage
   * @param {Object} options
   * @param {Number} options.usageLevel
   * @memberof Command
   */
  constructor(name, func, help, options) {
    this.name = name;
    this.func = func;
    this.help = help;
    this.options = options;
  }
}

module.exports = Command;
