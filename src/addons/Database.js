const mongoose = require("mongoose");
const config = require("../private/config.json");
const Logger = require("./Logger");

mongoose.connect(
  config.database.url,
  {
    dbName: config.database.name,
    useNewUrlParser: true
  },
  err => {
    if (err) throw Logger.error(err.stack);
    Logger.info("Connected to MongoDB");
  }
);

module.exports = mongoose.connection;
