const config = require("../private/config.json");
const GuildModel = require("../models/Guild");

module.exports = async (userID, guildID, Synapse) => {
  if (userID === config.ownerID) return 3;
  const guild = await GuildModel.findByGuildId(guildID);
  const user = Synapse.guilds
    .find(g => g.id === guild.id)
    .members.find(u => u.id === userID);
  if (user.permission.has("administrator")) return 2;
  if (user.roles.includes(guild.modRole)) return 1;
  return 0;
};
