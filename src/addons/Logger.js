const chalk = require("chalk").default;
const Sentry = require("./Sentry");

class Logger {
  /**
   * @param {String} log
   * @memberof Logger
   */
  info(log) {
    console.log(`${chalk.blue.bold("Info:")} ${log}`);
    return this;
  }
  /**
   * @param {String} log
   * @memberof Logger
   */
  error(log) {
    console.log(`${chalk.red.bold("Error:")} ${log}`);
    Sentry.captureException(log);
    return this;
  }
  /**
   * @param {String} log
   * @memberof Logger
   */
  warn(log) {
    console.log(`${chalk.yellow.bold("Warn:")} ${log}`);
    return this;
  }
}

module.exports = new Logger();
