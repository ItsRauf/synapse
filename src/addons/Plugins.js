const Logger = require("./Logger");

class Plugin {
  constructor(name) {
    this.name = name;
    this.commands = new Map();
  }
  async addCommand(cmd) {
    this.commands.set(cmd.name, cmd);
  }
  async getCommand(name) {
    return this.commands.get(name);
  }
}

const Plugins = [];

// module.exports = Plugins;

module.exports.loadPlugins = async () => {
  const fs = require("fs");
  fs.readdir("./src/plugins", async (err, folders) => {
    if (err) return console.error(err.stack);
    for (let folder in folders) {
      folder = folders[folder];
      fs.lstat(`./src/plugins/${folder}`, async (err, stats) => {
        if (err) return console.error(err.stack);
        if (stats.isDirectory()) {
          const plugin = new Plugin(folder);
          Logger.info(`Plugin: ${plugin.name}`);
          fs.readdir(`./src/plugins/${folder}`, async (err, files) => {
            if (err) return console.error(err.stack);
            for (let file in files) {
              file = files[file];
              const cmd = require(`../plugins/${folder}/${file}`);
              plugin
                .addCommand(cmd)
                .then(() => Logger.info(`Command: ${cmd.name}`));
            }
            Plugins.push(plugin);
          });
        }
      });
    }
  });
  return Plugins;
};
