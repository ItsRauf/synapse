module.exports = err => {
  return {
    embed: {
      title: `Error`,
      description: err,
      color: 0x36393e
    }
  };
};
