// Node Modules
require("dotenv").config({
  path: require("path").resolve(process.cwd(), "src/private/.env")
});
const Eris = require("eris");
require("eris-additions")(Eris);

// Addons
const Logger = require("./addons/Logger");
const { loadPlugins } = require("./addons/Plugins");
const Database = require("./addons/Database");
const calcUsageLevel = require("./addons/calcUsageLevel");
// const Sentry = require("./addons/Sentry");

// Models
const GuildModel = require("./models/Guild");
const UserModel = require("./models/User");

// Client Configuration
const config = require("./private/config.json");
const Synapse = new Eris.Client(config.token, {
  defaultImageFormat: "png",
  defaultImageSize: 512,
  getAllUsers: true,
  restMode: true,
  autoreconnect: true
});
Synapse.database = Database;

Synapse.on("ready", () => {
  Logger.info("Connected to Discord");
  // Load Plugins
  loadPlugins().then(Plugins => (Synapse.plugins = Plugins));
});

Synapse.on("messageCreate", async msg => {
  try {
    // Get Guild
    const guild = await GuildModel.findByGuildId(msg.channel.guild.id);
    // Do nothing if blacklisted
    if (guild.blacklisted) return;
    // Get User
    const user = await UserModel.findByUserId(msg.author.id);
    // Do nothing if blacklisted
    if (user.blacklisted) return;
    // Check if message starts with guild prefix or default prefix
    if (
      msg.content.startsWith(guild.prefix) ||
      msg.content.startsWith(config.prefix)
    ) {
      // Split message into command string and args
      let split = msg.content.split(" ");
      // Extract command name form command string using guild prefix or default prefix
      let cmdName =
        split[0].slice(guild.prefix.length) ||
        split[0].slice(config.prefix.length);
      // Shift to remove 1st element (command name)
      split.shift();
      let args = split;
      // Get plugin that has command
      const plugin = Synapse.plugins.find(plugin =>
        plugin.commands.has(cmdName)
      );
      if (plugin) {
        // Check if user can run the command
        let usageLevel = await calcUsageLevel(
          msg.author.id,
          msg.channel.guild.id,
          Synapse
        );
        // Get command
        const cmd = await plugin.getCommand(cmdName);
        // Run command
        if (usageLevel >= cmd.options.usageLevel) {
          cmd.func(msg, args, Synapse);
        }
      }
    }
  } catch (err) {
    // Catch error
    Logger.error(err);
  }
});

Synapse.on("guildCreate", async guild => {
  // await GuildModel.findByGuildId(guild.id)
  Synapse.createMessage(guild.systemChannelID, "Thanks for inviting Synapse!");
});

Synapse.connect().catch(err => Logger.error(err));

module.exports = Synapse;
