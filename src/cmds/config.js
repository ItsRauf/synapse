const Command = require("../addons/Command");
const Logger = require("../addons/Logger");
const GuildModel = require("../models/Guild");
const config = require("../private/config.json");

module.exports = new Command(
  "config",
  async (msg, args, Synapse) => {
    try {
      switch (args[0]) {
        case "reset":
          GuildModel.findOne(
            {
              id: msg.channel.guild.id
            },
            (err, guild) => {
              if (err) return Logger.error(err);
              guild.config = {
                prefix: config.prefix,
                blacklisted: false
              };
              guild.markModified("config");
              guild.save();
              msg.channel.createMessage({
                embed: {
                  title: `Config Reset for ${msg.channel.guild.name} (${
                    msg.channel.guild.id
                  })`,
                  color: 0x36393e,
                  description: "Default configuration has been restored"
                }
              });
            }
          );
          break;
        case "set":
          GuildModel.findOne(
            {
              id: msg.channel.guild.id
            },
            (err, guild) => {
              if (err) return Logger.error(err);
              guild.config[args[1]] = args[2];
              guild.markModified("config");
              guild.save();
              msg.channel.createMessage({
                embed: {
                  title: `Config Updated for ${msg.channel.guild.name} (${
                    msg.channel.guild.id
                  })`,
                  color: 0x36393e,
                  description: `\`\`\`js\n${args[1]}: ${
                    guild.config[args[1]]
                  }\`\`\``
                }
              });
            }
          );
          break;

        case "get":
          GuildModel.findOne(
            {
              id: msg.channel.guild.id
            },
            (err, guild) => {
              if (err) return Logger.error(err);
              msg.channel.createMessage({
                embed: {
                  title: `${args[1]} for ${msg.channel.guild.name} (${
                    msg.channel.guild.id
                  })`,
                  color: 0x36393e,
                  description: `\`\`\`js\n${guild.config[args[1]]}\`\`\``
                }
              });
            }
          );
          break;

        default:
          GuildModel.findOne(
            {
              id: msg.channel.guild.id
            },
            (err, guild) => {
              if (err) return Logger.error(err);
              msg.channel.createMessage({
                embed: {
                  title: `Config for ${msg.channel.guild.name} (${
                    msg.channel.guild.id
                  })`,
                  color: 0x36393e,
                  description: `\`\`\`js\n${JSON.stringify(
                    guild.config,
                    null,
                    2
                  )}\`\`\``
                }
              });
            }
          );
          break;
      }
    } catch (error) {
      Logger.error(error);
    }
  },
  {
    description: "Configuration for Synapse",
    usage: ["config", "config set [item] [value]", "config get [item]"]
  }
);
