const mongoose = require("mongoose");
const config = require("../private/config.json");
const Logger = require("../addons/Logger");

const GuildSchema = new mongoose.Schema({
  id: {
    type: String
  },
  prefix: {
    type: String,
    default: config.prefix
  },
  blacklisted: {
    type: Boolean,
    default: false
  },
  modRole: {
    type: String
  }
});

const Guild = mongoose.model("Guilds", GuildSchema);

module.exports = Guild;
module.exports.findByGuildId = id => {
  return new Promise((res, rej) => {
    Guild.findOne(
      {
        id
      },
      (err, guild) => {
        if (err) rej(err);
        if (!guild) {
          Guild.create({ id }, err => {
            if (err) return rej(err);
            Logger.info(`Created Guild: ${id}`);
          });
        } else {
          res(guild);
        }
      }
    );
  });
};
