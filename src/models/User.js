const mongoose = require("mongoose");
const Logger = require("../addons/Logger");

const UserSchema = new mongoose.Schema({
  id: {
    type: String
  },
  infractions: {
    type: Array,
    default: []
  },
  blacklisted: {
    type: Boolean,
    default: false
  }
});

const User = mongoose.model("Users", UserSchema);

module.exports = User;
module.exports.findByUserId = id => {
  return new Promise((res, rej) => {
    User.findOne(
      {
        id
      },
      (err, user) => {
        if (err) rej(err);
        if (!user) {
          User.create({ id }, err => {
            if (err) return rej(err);
            Logger.info(`Created User: ${id}`);
          });
        } else {
          res(user);
        }
      }
    );
  });
};
