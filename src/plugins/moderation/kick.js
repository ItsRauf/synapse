const Command = require("../../addons/Command");
const Logger = require("../../addons/Logger");
const ErrorEmbed = require("../../addons/ErrorEmbed");

module.exports = new Command(
  "kick",
  async (msg, args, Synapse) => {
    const SynapsePerm = msg.channel.guild.members
      .get(Synapse.user.id)
      .permission.has("kickMembers");
    const UserPerm = msg.channel.guild.members
      .get(msg.author.id)
      .permission.has("kickMembers");

    if (!SynapsePerm) {
      return msg.channel.createMessage(
        ErrorEmbed("Synapse does not have permission to kick members here!")
      );
    } else if (!UserPerm) {
      return msg.channel.createMessage(
        ErrorEmbed("You do not have permission to kick members here!")
      );
    }

    if (!args.length) {
      return msg.channel.createMessage(ErrorEmbed("Please provide a user id!"));
    }
    const member = msg.channel.guild.members.get(args[0]);

    if (Synapse.user.id === member.id) {
      return msg.channel.createMessage(
        ErrorEmbed("You are really trying to kick me!?!?")
      );
    } else if (msg.author.id === member.id) {
      return msg.channel.createMessage(ErrorEmbed("You can not kick yourself"));
    }

    if (!member.kickable) {
      return msg.channel.createMessage(
        ErrorEmbed(
          "This user is not kickable due to permissions which allow them not to be kicked."
        )
      );
    } else {
      Synapse.kickGuildMember(msg.channel.guild.id, args[0]).then(() => {
        msg.channel.createMessage({
          embed: {
            title: `Kicked User ${args[0]}`,
            color: 0x36393e
          }
        });
      });
    }
  },
  {
    description: "Kicks a user from a Server",
    usage: "kick [user.id]"
  },
  {
    usageLevel: 1
  }
);
