const Command = require("../../addons/Command");
const Logger = require("../../addons/Logger");

module.exports = new Command(
  "clean",
  async (msg, args, Synapse) => {
    try {
      let clean = async msgs => {
        let ids = msgs.map(msg => msg.id);
        let invalid = ids.filter(
          id => id < (Date.now() - 1421280000000) * 4194304
        );
        let messages = ids.splice(0, ids.length - invalid.length);
        return messages;
      };
      let ids;
      if (!args[0]) {
        let messages = await Synapse.getMessages(msg.channel.id, 10);
        ids = await clean(messages);
      } else {
        let messages = await Synapse.getMessages(msg.channel.id, args[0]);
        ids = await clean(messages);
      }
      await Synapse.deleteMessages(msg.channel.id, ids, "Synapse Clean");
      msg.channel
        .createMessage({
          embed: {
            title: `Clean`,
            description: `Cleaned ${ids.length} Messages`,
            author: {
              name: Synapse.user.username,
              icon_url: Synapse.user.avatarURL
            },
            color: 0x36393e,
            footer: {
              text: `Requested by ${msg.author.username}#${
                msg.author.discriminator
              }`
            }
          }
        })
        .then(msg => setTimeout(() => msg.delete(), 3000));
    } catch (error) {
      Logger.error(error);
    }
  },
  {
    description: "Deletes specified amount of messages",
    usage: "clean [amount]"
  },
  {
    usageLevel: 1
  }
);
