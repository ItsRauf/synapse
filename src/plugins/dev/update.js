const Command = require("../../addons/Command");
const Logger = require("../../addons/Logger");

module.exports = new Command(
  "update",
  async (msg, args, Synapse) => {
    try {
      const ChildProcess = require("child_process");
      ChildProcess.exec("git pull origin master", (err, stdout, stderr) => {
        if (err) Logger.error(err)
        // if (stderr) Logger.error(stderr)
        // Logger.info(stdout)
        msg.channel.createMessage(stdout)
      })

    } catch (err) {
      Logger.error(err)
    }
  },
  {
    description: "Pulls update from Gitlab",
    usage: "pull [-r]"
  },
  {
    usageLevel: 3
  }
);
