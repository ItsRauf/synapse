const Command = require("../../addons/Command");
const Logger = require("../../addons/Logger");

module.exports = new Command(
  "restart",
  async (msg, args, Synapse) => {
    msg.channel.createMessage("Restarting!").then(() => {
      process.send("restart", null, { swallowErrors: false }, err => {
        if (err) msg.channel.createMessage(err);
      });
    });
  },
  {
    description: "Restarts Synapse",
    usage: "restart"
  },
  {
    usageLevel: 3
  }
);
