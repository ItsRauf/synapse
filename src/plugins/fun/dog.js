const Command = require("../../addons/Command");
const Logger = require("../../addons/Logger");
const https = require("https");

module.exports = new Command(
  "dog",
  async (msg, args, Synapse) => {
    try {
      https.get(
        "https://api.thedogapi.com/v1/images/search?size=large",
        res => {
          res.on("data", d => {
            let url = JSON.parse(d.toString("ascii"))[0].url;
            msg.channel.createMessage({
              embed: {
                image: {
                  url: url
                }
              }
            });
          });
        }
      );
    } catch (error) {
      Logger.error(error);
    }
  },
  {
    description: "Returns an image of a dog",
    usage: "dog"
  },
  {
    usageLevel: 0
  }
);
