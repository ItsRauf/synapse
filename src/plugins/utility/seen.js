const Command = require("../../addons/Command");
const Logger = require("../../addons/Logger");

module.exports = new Command(
  "seen",
  async (msg, args, Synapse) => {
    try {
      let guilds = Synapse.guilds.filter(g => g.members.has(args[0]));
      // console.log(guilds);
      let channels = guilds.map(g => g.channels);
      let messageChannels = [];
      channels.forEach(c => {
        c.forEach(mc => {
          if (mc.messages) {
            messageChannels.push(mc);
          }
        });
      });
      let messages = messageChannels.map(mc => {
        let messages = mc.messages.filter(m => m.author.id === args[0]);
        if (messages.length >= 1) {
          return messages;
        }
      });
      messages = messages
        .reduce((acc, val) => acc.concat(val), [])
        .filter(m => m !== undefined);
      let timestamps = messages.map(m => m.timestamp).sort();
      let lastSeen = timestamps[timestamps.length - 1];
      console.log(new Date(lastSeen));
    } catch (error) {
      Logger.error(error);
    }
  },
  {
    description:
      "Returns timestamp of when Synapse last saw the specified user",
    usage: "seen [user.id]"
  },
  {
    usageLevel: 0
  }
);
