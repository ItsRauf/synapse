const Command = require("../../addons/Command");
const Logger = require("../../addons/Logger");
const UserModel = require("../../models/User");

module.exports = new Command(
  "uinfo",
  async (msg, args, Synapse) => {
    try {
      let sendMemberEmbed = async user => {
        let member = await Synapse.getRESTGuildMember(
          msg.channel.guild.id,
          user.id
        );
        let u = await Synapse.getRESTUser(member.user.id);
        msg.channel.createMessage({
          embed: {
            title: `Info on ${member.username}#${member.discriminator} (${
              member.user.id
            })`,
            thumbnail: {
              url: u.avatarURL
            },
            color: 0x36393e,
            fields: [
              {
                name: "User",
                value: `${member.user.username}#${member.user.discriminator}`,
                inline: true
              },
              {
                name: "User ID",
                value: member.user.id,
                inline: true
              },
              {
                name: "Joined At",
                value: new Date(member.joinedAt).toString()
              },
              {
                name: "Created At",
                value: new Date(member.user.createdAt).toString()
              },
              {
                name: "Infraction Count",
                value: user.infractions.length,
                inline: true
              },
              {
                name: "Blacklisted",
                value: user.blacklisted,
                inline: true
              }
            ]
          }
        });
      };
      let sendUserEmbed = async id => {
        let user = await Synapse.getRESTUser(id);
        msg.channel.createMessage({
          embed: {
            title: `Info on ${user.username}#${user.discriminator} (${
              user.id
            })`,
            thumbnail: {
              url: user.avatarURL
            },
            color: 0x36393e,
            fields: [
              {
                name: "User",
                value: `${user.username}#${user.discriminator}`,
                inline: true
              },
              {
                name: "User ID",
                value: user.id,
                inline: true
              },
              {
                name: "Created At",
                value: new Date(user.createdAt).toString()
              }
            ]
          }
        });
      };
      if (!args.length) {
        const user = await UserModel.findByUserId(msg.author.id);
        sendMemberEmbed(user);
      } else {
        const user = await UserModel.findByUserId(args[0]);
        if (user) {
          sendMemberEmbed(user);
        } else {
          sendUserEmbed(args[0]);
        }
      }
    } catch (error) {
      Logger.error(error);
    }
  },
  {
    description: "Info on a user",
    usage: "uinfo [user.id?]"
  },
  {
    usageLevel: 0
  }
);
