const Command = require("../../addons/Command");
const Logger = require("../../addons/Logger");
const GuildModel = require("../../models/Guild");
const calcUsageLevel = require("../../addons/calcUsageLevel");

module.exports = new Command(
  "help",
  async (msg, args, Synapse) => {
    try {
      let cmds = Synapse.plugins
        .map(plugin => Array.from(plugin.commands.values()))
        .reduce((acc, val) => acc.concat(val), []);
      cmds = cmds.filter(cmd => !cmd.options.disabled);
      let usageLevel = await calcUsageLevel(
        msg.author.id,
        msg.channel.guild.id,
        Synapse
      );
      cmds = cmds.filter(cmd => usageLevel >= cmd.options.usageLevel);
      let names = cmds.map(cmd => cmd.name);
      if (!args.length) {
        msg.channel.createMessage({
          embed: {
            title: "List of commands",
            description: `${names.join("\n")}`,
            color: 0x36393e
          }
        });
      } else {
        GuildModel.findOne(
          {
            id: msg.channel.guild.id
          },
          (err, guild) => {
            if (err) return Logger.error(err);
            let cmd = cmds.find(cmd => cmd.name === args[0]);
            let usage;
            if (Array.isArray(cmd.help.usage)) {
              usage = cmd.help.usage
                .map(usage => `${guild.config.prefix}${usage}`)
                .join("\n");
            } else {
              usage = `${guild.config.prefix}${cmd.help.usage}`;
            }
            msg.channel.createMessage({
              embed: {
                title: cmd.name,
                description: cmd.help.description,
                color: 0x36393e,
                fields: [
                  {
                    name: "Usage",
                    value: usage
                  }
                ]
              }
            });
          }
        );
      }
    } catch (error) {
      Logger.error(error);
    }
  },
  {
    description: "Help Command",
    usage: "help [command]"
  },
  {
    usageLevel: 0
  }
);
