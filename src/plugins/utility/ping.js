const Command = require("../../addons/Command");
const Logger = require("../../addons/Logger");

module.exports = new Command(
  "ping",
  async (msg, args, Synapse) => {
    try {
      const shard = await msg.channel.guild.shard;
      const message = await msg.channel.createMessage("Pong!");
      await message.edit({
        content: "",
        embed: {
          title: `\nPong! :ping_pong: `,
          description: `\nBot Ping: ${message.timestamp -
            msg.timestamp} ms\nAPI Ping: ${shard.lastHeartbeatReceived -
            shard.lastHeartbeatSent}ms`,
          author: {
            name: Synapse.user.username,
            icon_url: Synapse.user.avatarURL
          },
          color: 0x36393e,
          footer: {
            text: `Requested by ${msg.author.username}#${
              msg.author.discriminator
            }`
          }
        }
      });
    } catch (error) {
      Logger.error(error);
    }
  },
  {
    description: "Checks if the bot is alive",
    usage: "ping"
  },
  {
    usageLevel: 0
  }
);
