const Command = require("../../addons/Command");
const Logger = require("../../addons/Logger");
const GuildModel = require("../../models/Guild");

module.exports = new Command(
  "ginfo",
  async (msg, args, Synapse) => {
    try {
      let sendGuildEmbed = async guild => {
        let g = await Synapse.guilds.get(guild.id);
        msg.channel.createMessage({
          embed: {
            title: `Info on ${g.name} (${g.id})`,
            thumbnail: {
              url: g.iconURL
            },
            color: 0x36393e,
            fields: [
              {
                name: "Name",
                value: g.name,
                inline: true
              },
              {
                name: "Owner",
                value: `<@${g.ownerID}> (${g.ownerID})`,
                inline: true
              },
              {
                name: "User Count",
                value: g.members.filter(m => !m.bot).length
              },
              {
                name: "Bot Count",
                value: g.members.filter(m => m.bot).length,
                inline: true
              },
              {
                name: "Roles",
                value: g.roles.map(r => r.name).join(", ")
              }
            ]
          }
        });
      };
      if (!args.length) {
        const guild = await GuildModel.findByGuildId(msg.channel.guild.id);
        sendGuildEmbed(guild);
      }
    } catch (error) {
      Logger.error(error);
    }
  },
  {
    description: "Returns information on a guild",
    usage: "ginfo"
  },
  {
    usageLevel: 0
  }
);
