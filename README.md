# Synapse

Synapse is a multi-functional discord bot catering to your every need.

Invite URL: https://discordapp.com/api/oauth2/authorize?client_id=577342130981437442&permissions=284519543&scope=bot

### Commands

`about` - Information about the bot  
`help` - List of Commands / Command  
`eval` - Runs Javascript Snippet

### Installation

- Install NodeJS from https://nodejs.org
- Clone repo using `git clone https://gitlab.com/ItsRauf/synapse.git`
- Install dependencies using `npm install`
- Copy `src/private/example.config.json` to `src/private/config.json`
- Edit config to add token, prefix, and database information
- Run the bot with `npm start`

---

Synapse Discord Server: discord.gg/
